package jc.scratch.camel.exception;

public class UploadException extends Exception {
    public UploadException(Throwable t) {
        super(t);
    }
}
