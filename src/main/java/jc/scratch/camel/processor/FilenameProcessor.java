package jc.scratch.camel.processor;

import jc.scratch.camel.payload.UploadPayload;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Component;

@Component("filename")
public class FilenameProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        String fileName = exchange.getProperty(Exchange.FILE_NAME, String.class);
        fileName = buildFilename(fileName);
        exchange.setProperty(Exchange.FILE_NAME, fileName);
        UploadPayload payload = exchange.getIn().getBody(UploadPayload.class);
        exchange.getOut().setBody(payload.withFileName(fileName));
    }

    private String buildFilename(String fileName) {
        String baseName = FilenameUtils.getBaseName(fileName);
        if (StringUtils.isNotBlank(baseName)) {
            baseName = baseName + "-";
        }
        String timestamp = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss");
        String ext = FilenameUtils.getExtension(fileName);
        if (StringUtils.isNotBlank(ext)) {
            ext = "." + ext;
        }
        return String.format("%s%s%s", baseName, timestamp, ext);
    }
}
