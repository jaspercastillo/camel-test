package jc.scratch.camel.processor;

import jc.scratch.camel.payload.UploadPayload;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.converter.IOConverter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

@Component("multipart")
public class MultipartProcessor implements Processor {
    private static final String PART_FILE = "file";

    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getHeader(Exchange.HTTP_SERVLET_REQUEST, HttpServletRequest.class);
        Part part = request.getPart(PART_FILE);

        byte[] data = IOConverter.toBytes(part.getInputStream());
        String fileName = part.getSubmittedFileName();
        long size = part.getSize();
        String contentType = part.getContentType();

        exchange.setProperty(Exchange.FILE_NAME, fileName);
        exchange.getOut().setBody(new UploadPayload(data, fileName, size, contentType));
    }
}
