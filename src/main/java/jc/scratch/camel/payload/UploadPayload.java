package jc.scratch.camel.payload;

import lombok.Value;

@Value
public class UploadPayload {
    private byte[] data;
    private String fileName;
    private long size;
    private String contentType;

    public UploadPayload withFileName(String fileName) {
        return new UploadPayload(data, fileName, size, contentType);
    }
}
