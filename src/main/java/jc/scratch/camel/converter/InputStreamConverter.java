package jc.scratch.camel.converter;

import jc.scratch.camel.payload.UploadPayload;
import org.apache.camel.Converter;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Converter
public class InputStreamConverter {
    @Converter
    public static InputStream toInputStream(UploadPayload payload) {
        return new ByteArrayInputStream(payload.getData());
    }
}
