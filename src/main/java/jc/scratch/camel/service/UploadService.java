package jc.scratch.camel.service;

import jc.scratch.camel.exception.UploadException;
import jc.scratch.camel.payload.UploadPayload;

public interface UploadService {
    String upload(UploadPayload payload) throws UploadException;
}
