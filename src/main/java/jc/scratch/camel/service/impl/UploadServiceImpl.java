package jc.scratch.camel.service.impl;

import jc.scratch.camel.exception.UploadException;
import jc.scratch.camel.payload.UploadPayload;
import jc.scratch.camel.service.UploadService;
import org.apache.camel.Body;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service("upload")
public class UploadServiceImpl implements UploadService {
    public String upload(@Body UploadPayload payload) throws UploadException {
        System.out.println("filename=" + payload.getFileName());
        try (InputStream is = new ByteArrayInputStream(payload.getData())) {
            IOUtils.copy(is, System.out);
        } catch (IOException e) {
            throw new UploadException(e);
        }
        return payload.getFileName() + ":" + payload.getContentType() + ":" + payload.getSize();
    }
}
