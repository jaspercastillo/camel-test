package jc.scratch.camel.routes;

import jc.scratch.camel.service.UploadService;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Slf4j
@Component
public class UploadRouteBuilder extends RouteBuilder {
    @Value("${dir.upload:/temp/upload}")
    private String uploadDirectory;

    @Inject
    private UploadService uploadService;

    @PostConstruct
    private void init() {
        log.info("Upload directory set: {}", uploadDirectory);
    }

    @Override
    public void configure() throws Exception {
        // access via POST http://localhost:8080/camel/upload
        rest()
                .post("/upload").route()
                    .routeId("rest-upload")
                    // process multipart data into UploadPayload
                    .processRef("multipart")
                    // generate filename
                    .processRef("filename")
                    // write file
                    .toF("file://%s?fileName=${exchangeProperty.CamelFileName}", uploadDirectory)
                    // process UploadPayload and respond
                    .bean(uploadService);
    }
}
